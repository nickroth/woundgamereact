# The Wound Game

The Wound Game app is inspired by the Wound Game that Triangle Sword Guild has played at some of our past events. It is designed to simulate the impact that certain wounds historically and/or medically would do to an opponent. The goal is to survive.

## General Idea

Many different tournament rule sets emphasize different types of hits and targets. Generally the more "lethal" a hit is, the more points a fighter would get. Here we've tried to reconstruct how long a fighter might live (if at all) after taking such a wound and then turned that into a little game.

## How To Play

Each match go roughly like this:

1. Find two fighters and at least a third person to run the fight. Additional judges are up to your discretion.
1. Have the fighters fence until some hit of quality happens, then reset the fighters.
1. Announce to both fighters what the hits are. Mark in the app what kind of hit happened against each fighter.
1. The app will determine how much time left each fighter has. Unless one fighter is already out, **do NOT inform the fighters of either's remaining time.**
1. If both are still alive, continue scoring exchanges (starting and stopping the timer as needed) until someone either (a) concedes the match or (b) is incapacitated.

When using this match style in a tournament-ish way, fighters may bow out at any point in their match to live to fight again. The fighter that survived and didn't concede gets a win. The conceding fighter is still in the tournament but just didn't get a win. A fighter is only eliminated from the tournament if they die in a match. Wounds do not carry across matches. The winner of such a tournament is whoever can survive to the end AND gather as many wins across a set number of matches. Other places are also determined by surviving and number of wins.

## Survival Odds and Times

Ben Strickling is credited with the historical/medical research for generating the probabilities. Thank him and then you go ask him where he got these numbers. I'm just the messenger.

In the spirit of the game I won't publish the exact details of the probabilities of each hit here. If you simply must know the details, all the code is open source here and you can find it. ... You cheaty pants person you.

Similar probability descriptions means the probabilities are close, and not guaranteed to be exactly the same.

| Hit type | General result |
| ------ | ------ |
| Chest Thrust | Some chance of instant death, low chance of completely surviving, otherwise moderate survival time. |
| Other Torso Hit | Never instantly kills, moderate chance of entirely surviving. |
| Neck Hit | Some chance of instant death, a bit less survivable than a Chest Thrust. |
| Head Hit | Some chance of instant death, a bit more survivable than a Chest Thrust. |
| Face Hit | Likely chance of instant death, otherwise just as survivable as a Head Hit. |
| Limb Cut | Like chance of a simple flesh wound. Low chance of instant death. Otherwise quite survivable. |
| Limb Thrust | No chance of instant death. Mostly survivable. |
| Hand Hit | Some chance of instant incapacitation. Less of a chance of no damage at all. Otherwise quite surivable. |

