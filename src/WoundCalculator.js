const randomHit = () => Math.floor(Math.random() * 100);
const randomSurvival = (from, to) => from + Math.floor(Math.random() * (to - from));

export const dead = () => 0;
export const survive = () => -1;

export const chestThrust = () => {
    const hit = randomHit();
    if (hit < 20) return dead();
    if (hit < 70) return randomSurvival(5, 10);
    if (hit < 90) return randomSurvival(10, 30)
    return survive();
}

export const torsoHit = () => {
    const hit = this.randomHit();
    if (hit < 4) return this.randomSurvival(30, 60)
    return survive();
}

export const neckHit = () => {
    const hit = randomHit();
    if (hit < 25) return dead();
    if (hit < 75) return randomSurvival(2, 10);
    if (hit < 90) return randomSurvival(10, 30);
    return survive();
}

export const headHit = () => {
    const hit = randomHit()
    if (hit < 30) return dead();
    if (hit < 55) return randomSurvival(10, 20);
    if (hit < 75) return randomSurvival(20, 60);
    return survive();
}

export const faceHit = () => {
    const hit = randomHit();
    if (hit < 50) return dead();
    if (hit < 75) return randomSurvival(10, 20);
    if (hit < 90) return randomSurvival(20, 60);
    return survive();
}

export const limbCut = () => {
    const hit = randomHit();
    if (hit < 10) return dead();
    if (hit < 30) return randomSurvival(10, 30);
    if (hit < 50) return randomSurvival(30, 60);
    return survive();
}

export const limbThrust = () => {
    const hit = randomHit();
    if (hit < 40) return randomSurvival(10, 40);
    return survive();
}

export const handHit = () => {
    const hit = randomHit();
    if (hit < 30) return dead();
    if (hit < 55) return randomSurvival(10, 30);
    if (hit < 80) return randomSurvival(30, 90);
    return survive();
}

export default {
    chestThrust,
    torsoHit,
    neckHit,
    headHit,
    faceHit,
    limbCut,
    limbThrust,
    handHit
};