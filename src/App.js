import React, { Component } from 'react';
import './App.css';

import WoundFunctions from "./WoundCalculator";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            timerIsRunning: false,
            blueStatus: -1,
            redStatus: -1,
            blueDisplayStatus: "Alive and well",
            redDisplayStatus: "Alive and well",
            lastUpdate: Date.now(),
            timerId: null,
            blueHitType: "chestThrust",
            redHitType: "chestThrust"
        };
    }

    timerStartStop() {
        if (this.state.timerIsRunning) {
            this.stopTimer();
        } else {
            this.startTimer();
        }
    }

    startTimer() {
        const timerId = setInterval(this.onTimerTick.bind(this), 50);
        this.setState({
            timerIsRunning: true,
            timerId,
            lastUpdate: Date.now()
        });
    }

    stopTimer() {
        clearInterval(this.state.timerId)
        this.setState({timerIsRunning: false});
    }

    onTimerTick() {
        if (!this.state.timerIsRunning) return; // needed?

        const now = Date.now();
        const updateTimeDiff = now - this.state.lastUpdate;
        const nextBlueStatus = this.nextTickStatus(this.state.blueStatus, updateTimeDiff);
        const nextRedStatus = this.nextTickStatus(this.state.redStatus, updateTimeDiff);
        this.setState({
            lastUpdate: now,
            blueStatus: nextBlueStatus,
            blueDisplayStatus: this.neckTickDisplayStatus(nextBlueStatus),
            redStatus: nextRedStatus,
            redDisplayStatus: this.neckTickDisplayStatus(nextRedStatus)
        });

        if (nextBlueStatus === 0 || nextRedStatus === 0) {
            this.stopTimer();
        }
    }

    nextTickStatus(currentStatus, timeDiff) {
        if (currentStatus === -1 ) return -1;
        if (currentStatus === 0) return 0;
        if (timeDiff > currentStatus) return 0;
        return currentStatus - timeDiff;
    }

    neckTickDisplayStatus(status) {
        if (status === -1) return "Alive and well";
        if (status === 0) return "Dead :(";
        const seconds = Math.floor(status / 1000);
        const tenths = Math.floor((status - (seconds * 1000)) / 100);
        return `${seconds}.${tenths}s`;
    }

    onBlueHitChange(event) {
        this.setState({ blueHitType: event.target.value });
    }

    onRedHitChange(event) {
        this.setState({ redHitType: event.target.value });
    }

    registerBlueHit() {
        const nextStatus = this.nextHitStatus(this.state.blueStatus, this.state.blueHitType);
        this.setState({
            blueStatus: nextStatus,
            blueDisplayStatus: this.nextHitDisplayStatus(nextStatus)
        });
    }

    registerRedHit() {
        const nextStatus = this.nextHitStatus(this.state.redStatus, this.state.redHitType);
        this.setState({
            redStatus: nextStatus,
            redDisplayStatus: this.nextHitDisplayStatus(nextStatus)
        });
    }

    nextHitStatus(currentStatus, hitType) {
        const nextHit = WoundFunctions[hitType]();
        if (currentStatus === 0) return 0;
        if (nextHit === -1) return currentStatus;
        if (currentStatus === -1) return nextHit * 1000;
        return Math.min(currentStatus, nextHit * 1000);
    }

    nextHitDisplayStatus(status) {
        if (status === -1) return "Survived!";
        if (status === 0) return "Dead :(";
        const seconds = Math.floor(status / 1000);
        const tenths = Math.floor((status - (seconds * 1000)) / 100);
        return `${seconds}.${tenths}s`;
    }

    reset() {
        this.stopTimer();
        this.setState({
            blueStatus: -1,
            blueDisplayStatus: "Alive and well",
            redStatus: -1,
            redDisplayStatus: "Alive and well",
        });
    }

    render() {
        return (
            <div className="App">
                <div className="row">
                    <button className="col btn btn-primary" onClick={this.timerStartStop.bind(this)}>
                        {this.state.timerIsRunning ? "Stop" : "Start"}
                    </button>
                </div>
                <div className="row mt-4">
                    <div className="col">
                        <div className="container">
                            <div className="row">
                                <span className="col">Blue fighter</span>
                            </div>
                            <div className="row">
                                <span className="col">
                                    <strong>
                                        {this.state.blueDisplayStatus}
                                    </strong>
                                </span>
                            </div>
                            <div className="row mt-1">
                                <select className="custom-select" onChange={this.onBlueHitChange.bind(this)}>
                                    <option value="chestThrust">Chest Thrust</option>
                                    <option value="torsoHit">Other Torso Hit</option>
                                    <option value="neckHit">Neck Hit</option>
                                    <option value="headHit">Head Hit</option>
                                    <option value="faceHit">Face Hit</option>
                                    <option value="limbCut">Limb Cut</option>
                                    <option value="limbThrust">Limb Thrust</option>
                                    <option value="handHit">Hand Hit</option>
                                </select>
                            </div>
                            <div className="row mt-3">
                                <button className="col btn btn-success" onClick={this.registerBlueHit.bind(this)}>Add hit</button>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="container">
                            <div className="row">
                                <span className="col">Red fighter</span>
                            </div>
                            <div className="row">
                                <span className="col">
                                    <strong>
                                        {this.state.redDisplayStatus}
                                    </strong>
                                </span>
                            </div>
                            <div className="row mt-1">
                                <select className="custom-select" onChange={this.onRedHitChange.bind(this)}>
                                    <option value="chestThrust">Chest Thrust</option>
                                    <option value="torsoHit">Other Torso Hit</option>
                                    <option value="neckHit">Neck Hit</option>
                                    <option value="headHit">Head Hit</option>
                                    <option value="faceHit">Face Hit</option>
                                    <option value="limbCut">Limb Cut</option>
                                    <option value="limbThrust">Limb Thrust</option>
                                    <option value="handHit">Hand Hit</option>
                                </select>
                            </div>
                            <div className="row mt-3">
                                <button className="col btn btn-success" onClick={this.registerRedHit.bind(this)}>Add hit</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <button className="col btn btn-primary" onClick={this.reset.bind(this)}>Reset</button>
                </div>
                <div className="row mt-5">
                    <a className="col" href="https://gitlab.com/nickroth/woundgamereact/blob/master/Instructions.md">How to play</a>
                </div>
            </div>
        );
    }
}

export default App;
