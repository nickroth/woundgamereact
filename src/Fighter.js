import React, { Component } from 'react';

class Fighter extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <span className="col">{this.props.name}</span>
                </div>
                <div className="row">
                    <span className="col">Fighter status</span>
                </div>
                <div className="row">
                    <button className="col btn btn-success">Add hit</button>
                </div>
            </div>
        );
    }
}

export default Fighter;